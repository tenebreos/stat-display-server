#!/usr/bin/python3

# cases: test notification processing code on the server.

import socket
import time

inputs = [
    "AAAA\nAAAA\n",
    "AAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAA\n",
    "AAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAA\n",
    "AAAA",
    "AAAA\n",
    "AAAA\nAAAA",
    "AAAA\n\n",
    "\n\n",
    "AAAAAAAAAAAAAAAA",
    "AAAAAAAAAAAAAAAAAAAA",
    "AAAAAAAAAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAA",
    ""
]

for i in inputs:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("192.168.1.59", 1337))
    s.send(bytes(i, 'utf-8'))
    s.close()
    time.sleep(3)
