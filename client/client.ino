#include <WiFi.h>
#include <LiquidCrystal.h>

// LCD configuration
uint8_t degreeSign[8] = {
    B01110,
    B10001,
    B10001,
    B10001,
    B01110,
    B00000,
    B00000,
    B00000,
};
const byte DegreeSign = 0;
const int RS = 33;
const int RW = 32;
const int E  = 25;
const int D4 = 12;
const int D5 = 14;
const int D6 = 27;
const int D7 = 26;
LiquidCrystal lcd(RS, RW, E, D4, D5, D6, D7);

// WiFi Configuration
const char *Ssid = "SpaceX";
const char *Psk = "JustReadTheInstructions112358";
/* const char *Ssid = "WLAN1-N90BNL"; */
/* const char *Psk = "Dm1eb946gY05td16"; */
/* const char *Ssid = "INetworkProvider"; */
/* const char *Psk = "02ad0a679ea4"; */
const char *Hostname = "notification-display-server";

WiFiServer server(1337);

//#define DEBUG

void PrintIpToLcd(IPAddress ip)
{
    lcd.clear();
    lcd.print("WiFi Connected!");
    lcd.setCursor(0, 1);
    lcd.print(ip);
}

void PrintToLcd(char *msg, int line)
{
    lcd.setCursor(0, line);
    lcd.print(msg);
}

void PrintMsg(char *msg)
{
    lcd.clear();
    PrintToLcd(msg, 0);
}

bool ConnectToNetwork(const char *ssid, const char *psk)
{
    WiFi.begin(Ssid, Psk);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        char msg[] = "No network connectivity!";
        Serial.println(msg);
        PrintMsg(msg);
        digitalWrite(LED_BUILTIN, HIGH);
        return false;
    } else {
        IPAddress localIp = WiFi.localIP();
        Serial.printf("Wifi Connected! IP: %d.%d.%d.%d\n",
                localIp[0], localIp[1], localIp[2], localIp[3]);
        PrintIpToLcd(localIp);
        digitalWrite(LED_BUILTIN, LOW);
        return true;
    }
}

void setup()
{
    Serial.begin(115200);
    lcd.begin(16, 2);
    lcd.createChar(0, degreeSign);
    WiFi.setHostname(Hostname);
    ConnectToNetwork(Ssid, Psk);

    server.begin();
}

#ifdef DEBUG
void PrintBuffer(char *buf_name, char *buf, size_t n)
{
    Serial.print(buf_name);
    Serial.print(": ");
    for (int i = 0; i < n; ++i) {
        Serial.print(buf[i], HEX);
        Serial.print(' ');
    }
    Serial.print('\n');
}
#endif

void loop()
{
    if (WiFi.status() != WL_CONNECTED) {
        digitalWrite(LED_BUILTIN, LOW);
        ConnectToNetwork(Ssid, Psk);
        return;
    }

    WiFiClient client = server.available();
    if (client) {
        char lines[2][17] = { "", "" };
        int line = 0;
        int i = 0;
        while (client.available()) {
            if (line > 1)
                client.stop();
            int c = client.read();
            if (c != '\n') {
                if (i < 16) {
                    lines[line][i++] = c;
                } else {
                    lines[line][i] = '\0';
                }
            } else {
                lines[line][i] = '\0';
                ++line;
                i = 0;
            }
        }
        lines[line][i] = '\0';
        /* Cases to handle:
           - binary data
        */
#ifdef DEBUG
        Serial.println("Request:");
        PrintBuffer("line 1", lines[0], 17);
        PrintBuffer("line 2", lines[1], 17);
        Serial.println(lines[0]);
        Serial.println(lines[1]);
#endif
        lcd.clear();
        PrintToLcd(lines[0], 0);
        PrintToLcd(lines[1], 1);
    }
}
