#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

char *atos(struct sockaddr *sockaddr);
double readTemp();
double readFreeMem();
void printAddrinfo(struct addrinfo *ptr);
int parseIp(const char *ipStr, const char *portStr, struct sockaddr_in *ptr,
            socklen_t *socklen);

#define BUF 100

int main(int argc, char *argv[])
{
    // Check argument correcteness
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <client_ip> <client_port>\n", argv[0]);
        exit(-1);
    }

    // Parse the arguments into an address structure
    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen;
    if (!parseIp(argv[1], argv[2], &clientAddress, &clientAddressLen)) {
        fprintf(stderr, "Invalid IP: %s\n", argv[1]);
        exit(-1);
    }

    // Open the socket
    int socketfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketfd == -1) {
        perror("socket()");
        exit(-1);
    }

    // Bind to local port 1337
    // TODO: make this a program argument
    // TODO: implement

    // Connect the socket
    // TODO: check if its needed to to this. write() complains about lacking an
    // address, even when connected beforehand.
    if (connect(socketfd, (struct sockaddr *)&clientAddress,
                clientAddressLen)) {
        perror("connect()");
        exit(-1);
    }

    char packetData[BUF];
    while (1) {
        // Read and format cpu temp and free memory data
        snprintf(packetData, BUF, "%0.1lf %0.2lf", readTemp(), readFreeMem());

        // Send the data
        int unsuccessfullConnections = 0;
        const int connectionLimit = 5;
        if (sendto(socketfd, packetData, strlen(packetData), 0,
                   (struct sockaddr *)&clientAddress, clientAddressLen)
            == -1) {
            int errval = errno;
            perror("sendto()");
            printf("error value: %d\n", errval); // This may overwrite errno.
            if (errval == ECONNREFUSED)
                printf("Display connection refused... Retry %d of %d\n",
                       unsuccessfullConnections++ + 1, connectionLimit);
            if ((errval != ECONNREFUSED && errval != ENETUNREACH)
                || (errval == ENETUNREACH
                    && unsuccessfullConnections == connectionLimit))
                exit(-1);
            sleep(4);
        }
        sleep(1);
        if (unsuccessfullConnections != 0) {
            unsuccessfullConnections = 0;
            puts("Display connection established!");
        }
    }

close_socket_exit:
    // Close socket
    if (close(socketfd)) {
        perror("close()");
        exit(-1);
    }
}

char *atos(struct sockaddr *sockaddr)
{
    static char addr[22];
    char port[6];
    struct sockaddr_in *adam = (struct sockaddr_in *)sockaddr;
    snprintf(port, sizeof(port), "%d", ntohs(adam->sin_port));
    snprintf(addr, sizeof(addr), "%s:%s", inet_ntoa(adam->sin_addr), port);
    return addr;
}

double readTemp()
{
    char filename[] = "/sys/devices/platform/coretemp.0/hwmon/hwmon5/temp1_input";
    FILE *temp1_input = fopen(filename, "r");
    if (temp1_input == NULL) {
        char errormsg[sizeof(filename) + 7];
        strcat(errormsg, "fopen(");
        strcat(errormsg, filename);
        strcat(errormsg, ")");
        perror(errormsg);
        exit(-1);
    }
    double temp;
    fscanf(temp1_input, "%lf", &temp);
    fclose(temp1_input);
    temp /= 1000;
    return temp;
}

double readFreeMem()
{
    FILE *meminfo = fopen("/proc/meminfo", "r");
    if (meminfo == NULL) {
        perror("fopen(/proc/meminfo)");
        exit(-1);
    }
    char key[BUF];
    unsigned long value = 0;
    // TODO: warning, the following format may not be correct in every context.
    do {
        fscanf(meminfo, "%s %lu kB\n", key, &value);
    } while (strcmp(key, "MemAvailable:"));
    fclose(meminfo);
    double memAvail = value / (1024.0 * 1024.0);
    return memAvail;
}

void printAddrinfo(struct addrinfo *ptr)
{
    printf("addrinfo(ai_flags=%d, ai_family=%d, ai_socktype=%d, ai_addr=%s)\n",
           ptr->ai_flags, ptr->ai_family, ptr->ai_socktype, atos(ptr->ai_addr));
}

int parseIp(const char *ipStr, const char *portStr, struct sockaddr_in *ptr,
            socklen_t *socklen)
{
    *socklen = 16;
    ptr->sin_family = AF_INET;
    ptr->sin_port = htons(atoi(portStr));
    return inet_aton(ipStr, &(ptr->sin_addr));
}
